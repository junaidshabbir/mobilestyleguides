
#PITO Objective-C coding Guide

This guide serves to outline the coding conventions, best practices and style for all Putitout Objective-C projects. The guide should always be adhered to, even in the case of small proof of concepts or prototype applications.

# Aim

The aim of this guide is to standardize the way in which Objective-C is written, which subsequently reduces friction in reading it, making it more maintainable and therefore of a better quality.

The guide does not aim to define an applications architecture or any implementation details, these are domain specific problems which should be driven by the software architects and developers.

# Resources
* [Cocoa Coding Guidelines](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CodingGuidelines/CodingGuidelines.html)
* [objc.io](http://www.objc.io/)
* [NSHipster](http://nshipster.com/)
* [iOS Dev Weekly](http://iosdevweekly.com/)
* [Github Trending Objective-C Repositories](https://github.com/trending?l=objective-c)
* [NSBlog](https://www.mikeash.com/pyblog/)

# Table of Contents
* [Xcode Project](#xcode-project)
* [File or Class Names](#file-or-class-names)
* [Header Organisation](#header-organisation)
* [Implementation Organisation](#implementation-organisation)
* [Types](#types)
* [Variables](#variables)
* [Classes and Methods](#classes-and-methods)
* [Properties](#properties)
* [Delegates](#delegates)
* [Localisation](#localisation)
* [Constants](#constants)
* [Fonts or Colors](#fonts-or-colors)
* [App Delegate](#app-delegate)
* [Base ViewController](#base-viewcontroller)
* [UINavigation Controller](#uinavigation-controller)
* [URLManager](#urlmanager)
* [Models](#models)
* [Categories](#categories)
* [Git](#git)
* [PODs](#pods)
* [Autolayouts](#autolayouts)
* [Unit Tests](#unit-tests)
* [UIAutomation Tests](#uiautomation-tests)
* [Future Revisions](#future-revisions)

# Xcode Project
* The underlining file and folder system should match the project structure inside of Xcode. If a group is created in Xcode an underlining folder should exist in the same place on the file system.

* The Xcode project structure should be taken on a case by case basis but for "larger" projects a base project should include the following:

AppName 
-  AppName Workspace
-  AppName Source
   -  Application (Will contain Appdelegate folder and main.m)
   -  Controllers
       -  Separate folder for each challenge and it will contain all viewcontrollers related to that challenge and cells .h,.m and .xib files. Rather than separate folder for progress, result and load separate folders
  -  Library
    -  Categories folder
     - Parent classes, helper classes folder
  -  Models
     - All model classes should be in this folder rather than maintaining separate folder for every model class
  -  Resources
     -  Separate folder for fonts
  * Xcassets _(images)_
  -  Vendor
     -Third party frameworks and libraries


* Xcassets, where available, should be used to manage image assets.

* Image files should be descriptive of their screen function but should avoid visually describing their look. Filenames should be all lower case and spaces in file names should be replaced with hyphens.

**Example**
```obj
prefix-context-type-name 
pio-donateview-cancer-research-logo
and for shared items something like
pio-uinavigation-button-back
```

# File or Class Names
All classes should have a prefix before their name - Apple recommends you use 3 characters, to avoid issues with name spaces. We should have a prefix “PIO” with all class names

**Example**
```obj
PIOThanksViewController.h 
PIOPrivacyViewController.m
PIOFaqCustomCell.h
PIOUser.h
```
* Common acronyms should be entirely uppercase in class, method, property and variable names. In the case above Id should be ID. This is inline with Apples own conventions.

## Header Organisation
* Avoid declaring methods in the header which are not publicly required.

* Set `IBOutlets` in the .m file, this should not be part of .h file

* Avoid `#import`ing in header files unless explicitly required. Instead opt for forward class declaration via `@class`. This leads to [cleaner headers and faster compile times](http://qualitycoding.org/file-dependencies/). The `@class` reference should be declared below the `#import` statements but above the `@interface` statement with a blank line separating each category.

* Constants should always be set between `#import` (below `@class`) and `@interface`.

**Example**
```objc
 #import <UIKit/UIKit.h>
 #import <Foundation/Foundation.h>

 @class MBProgressHUD;
 
 extern NSString * const PIOLoginReference;

 @interface PIOLoginViewController : UIViewController
```

* Always import framework headers first, followed by a blank line and then the class specific imports.

**Example**
```objc
 #import <UIKit/UIKit.h>
 #import <Foundation/Foundation.h>
 #import <CoreLocation/CoreLocation.h>
 
 #import "LoginViewController.h"
 #import "IntroductionViewController.h"
```

## Implementation Organisation
* Constants and `typdef` should always be set between `#import` and `@implementation` references.

* All definitions stored at the top of the implementation file should be logically grouped together and consistent, with a single line between each "block".

**Example**
```objc
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

NSString * const PIOLoginReference = @"loginRef";
NSString * const PIOLogoutReference = @"logoutRef";

typedef void (^PIOLoginStatusBlock)(PIOLoginStatus status);

typedef NS_ENUM (NSInteger, PIOAlertviewTag) {
    PIODirectDonationAlertTag = 0,
    PIOOfflineDonationAlertTag = 1,
    PIOSaveDonationAlertTag = 2,
    PIOTodoTabAlertTag = 4,
};
 
@implementation PIOLoginViewController
``` 

* Conditional and loop bracing should start on the same line and end on a new line.

**Example**
```objc
if (user.isLoggedIn) {
...
}
```

* Method level braces should always be included on their own line.

**Example**
```objc
 - (IBAction)submitLoginCredentials:(UIButton *)sender
{
...
}
```

* Always use braces for `if` / `for` / `while` / `else` etc. statements. Readability and consistency are a fair trade-off in all cases, even in the case of early returns.

**Example**
```objc
if (!user.isValid) {
...
}
```

* Use Pragma mark to separate method sets, the below set provides a breakdown of the default set which should be used, additional ones can be added as required.
 * Lifecyle – `viewDidLoad`, `init`, `dealloc` etc.
 * Navigation – For links to other Views / Storyboards - `prepareForSegue`, `performSegue` etc.
 * Public – All public methods required for the class to function.
 * Actions – `IBActions`
 * Protocols – named individually by their class name
 * Private / Utility – For any private methods and class specific utility methods

**Example**
```objc
#pragma mark – Lifecycle

- (void)viewDidLoad
{
...
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
...
}

#pragma mark – Public

- (void)submitLoginCredentials
{
...
}

#pragma mark – Actions

- (IBAction)submitLoginCredentials:(id)sender
{
...
}

#pragma mark – UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
...
}

#pragma mark – UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
...
}

#pragma mark – Private / Utility

- (void)clearFormTextFields
{
...
}
```

* Use method returns early to help keep conditional logic clear.

**Example**
```objc
- (void)processRequest
{
    if (!user.isValid) {
        return;
    }

    // continue with flow
}
```

* There should only be a one line space between methods for consistency as well as a one line space between the final method brace and the `@end` directive.

**Example**
```objc
- (void)hideAdditionalTextFields
{
...
}

- (void)clearFormTextFields
{
...
}

@end
```

* Method declarations should always have a space after the class vs instance indicator (`-`/`+`).

**Example**
```objc
+ (void)registerForPushNotifications;
- (void)clearDefaultRequestParameters;
```

## Types
* All Objective-C primitive types should be used over their C counterpart where available - this aids in future proofing code.

* `NSInteger` and `NSUInteger` should be used over int, the only exception is for loop indices.
```objc
for (int i = 0; i < 10; i++) {
...
}
```

* `CGFloat` should be used over float.

* The auxiliary and additional types such as `NSTimeInterval` should be used (where relevant) over doubles.

## Variables
* Variable names should always be camel cased with the first letter lowercase and the pointer prefixing the variable name.

**Example**
```objc
NSString *firstName;
NSString *lastName;
```

* Variables should be named verbosely in line with Apple’s conventions. *The only exception is for loop indices.

**Example**
```objc
UIColor *configuredColorForView;
```

* When declaring `IBOutlet` the variable name should also be suffixed with the control type - this avoids issues with ambiguity when referencing the object later.

**Example**
```objc
self.usernameTextField.text = @"Chuck Norris";
[self.loginFormScrollView setContentOffset:point animated:YES];
```

* Use appropriate prefixes when working with `typedef` structures

**Example**
```objc
typedef NS_ENUM (NSInteger, PIOAlertviewTag) {
    PIODirectDonationAlertTag = 0,
    PIOOfflineDonationAlertTag = 1,
    PIOSaveDonationAlertTag = 2,
    PIOTodoTabAlertTag = 4,
};
```

## Classes and Methods
* All View Controllers class names should be suffixed with ViewController.

**Example**
```objc
PIOLoginViewController.m
PIORegisterViewController.m
```

* Class factory methods should be created where relevant, in-line with Apple’s recommendation and best practices. This aids and simplifies the process of object creation by the client.

**Example**
```objc
+ (instanceType)dateWithTimeIntervalSinceNow:(NSTimeInterval)secs;
+ (instanceType)userWithFirstName:(NSString *)firstName lastName:(NSString *)lastName; 
```

* When invoking multiple methods on a single object limit to a maximum of 2 for readability and make sure there’s a space between each call.

**Example**
```objc
[PIODataManager sharedInstance] fetchFriends];
```
* Method names should not contain abbreviations like str instead of string, img instead of image etc.
* Your use of get in method names is incorrect. Where appropriate remove or substitute for fetch.
* Remove IBActions declarations 

## Properties
* Avoid declaring instance variables and opt for properties instead.

* Dot-notation should be used to access and mutate properties.

**Example**
```objc
user.firstName = @”John”;
[PIODataManager sharedInstance].clientID;
```

* Nonatomic first, for consistency.

**Example**
```objc
@property (nonatomic, strong) MPHostServiceBrowser *serviceBrowser;
@property (nonatomic, strong) NSMutableArray *connectionList;
```

* If values need to be set on a `readonly` property declared in a header file declare the property with the `readwrite` attribute in the .m file.

**Example**
```objc
@property (nonatomic, strong, readwrite) NSString *userID;
```
* When declaring BOOL properties they currently look like this:
* 
**Example**
```objc
@property (nonatomic, assign, getter=isIPhone5) BOOL iPhone5;
```
## Collections
* Collection literals should always be used for non mutable items such as `NSArray`, `NSDictionary` etc.

* Access collection literals using the short syntax and the mutable counterpart using the family of `objectAtIndex` methods. This makes it clear what type of collection is being accessed.

**Example**
```objc
NSString *user = self.userList[0]; 
NSString *user = [self.userList objectAtIndex:0];
```

* When working with large dictionaries (e.g. JSON responses) opt to create class representations (models) of the data instead, this will help with code completion and compiler checks and avoid any keyName mistakes.

## Delegates
* When defining `delegate` callbacks the format should be in line with the standard UIKit conventions which starts with the object responsible for the delegation, the auxiliary verb (will/should/did/has/should) and the event.

**Example**
```objc
scrollViewDidScroll:
webView:shouldStartLoadWithRequest:navigationType:
```

* `Delegate` / `Data Sources` can be declared in either code or `Interface Builder`, however the approach which is taken should be used consistently throughout the lifetime of the `ViewController` to avoid a mix and match of the two, which leads to confusion when debugging. For example if an `IBOutlet` has its `delegate` declared in `Interface Builder` then all other relevant objects should have their `delegates` set in `Interface Builder` too. `Interface Builder` objects which have their `delegate` declared in code should have these bundled together in an associated “setup” method.

## Localisation
* For apps across regions use NSLocalized string for all user displayed string output. You may optionally choose to use them in non-regional if also required because of the [flexibility it provides](http://nshipster.com/nslocalizedstring/).

**Example**
```objc
textField.placeholder = NSLocalizedString(@"Username", "Username Placeholder");
```

## Constants
* Opt to declare constants in `.h` & `.m` files as `static` constants and not `#define` – this should also include a header reference where relevant.

**Example**
```objc
// .h 
extern NSString * const PIORootURL;

// .m
static NSString * const PIORootURL = @"http://www.putitout.co.uk/";  
```
## Fonts or Colors
Should have the following methods for each font as a factory method

```
+ (UIFont *)PIO<fontName>;

+ (UIFont*)PIO<fontName>withSize:(CGFoat)size;

```
Should have the following methods for each custom color as a factory method
```
+ (UIColor *)PIO<loginLabel>Color;

```

## App Delegate

User related code should be into a separate singleton(s) that manages Notification, User Preferences and API access

**Example**
```
PIOProjectNameController, PIONotificationManager, PIOSharedPreference
```

## Base ViewController

Define PIOBaseViewController which should setup common functionality of ViewControllers like startLoadingView, stopLoadingView
and other common functionality that is used in most view controllers. Inherit all view controller from this base ViewController.

## UINavigation Controller

Use standard UINavigation Bar instead of custom implementation.

## URLManager

PIOURLManager should be created with .h and .m for declaring URLs. It should expose read only properties and functions which provide url’s for request.

## Models

Use models instead of Dictionaries to store properties, web request and responses.

**Example**
```
@interface PIOUser : NSObject

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *session;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;
- (NSDictionary *)convertToDictionary;

+ (void)loginWithCustomerNumber:(NSString *)customerNumber callBack:(void (^)(NSError *error, NSString *accessToken))block;
+ (void)fetchUserDetails:(NSString *)customerNumber callBack:(void (^)(NSError *error, PIOUser *user))block;

@end
```

## Categories

Create categories to define additional methods for common use among View Controllers.

**Example**
```
Additions of UIFont, UIColor, UIDevice, UIImage, NSDate, NSDateFormatter, NSString
```

## Git

**Branch Naming Convention**

Branch name should describes about developer and git issue number.
```
John Smith is going to starts working on git issue # 21 then a new feature branch name should be js-issue-21
```

**Commit Messages**

Now you have written some code in your branch, and are ready to commit. You want to make sure to write good, clean commit message. As important as the content of the change, is the content of the commit message describing it. A commit message should be short and comprehensive.

**Use Pull Requests**

Pull requests (PR) are an excellent tool for fostering code review and a good practice is for someone else to merge your code into the mainline, ensuring 2 sets of eyeballs review each feature.

**Avoiding code conflicts** 

Always make sure your feature branch should be up-to date from the latest develop.

## PODs

To manage all third party libraries and frameworks dependency, cocoa pods should be used instead of manually adding like AFNetworking. Some of the advantages are given below

1. Library code is stored within your project, wasting space.
2. There’s no central place where you can see all libraries that are available.
3. It can be difficult to find and update a library to a new version, especially if
several libraries need to be updated together.
4. Downloading and including libraries within your project may tempt you to make changes to the downloaded code and just leave it there (making it harder to update them later).

## Autolayouts

We should use this feature for adaptive layouts when we plan to give support for different iPhone device for example iPhone 4s,iPhone 5,6,6+. We should avoid using spring and struts which is old approach.

## Unit Tests

We should write one unit test even though it is not requirement from client. You write one unit test to verify that login outlets are connected to code.

## UIAutomation Tests

One simple UIAutomation test should be written for example user registeration for forgot password flow.

## Future Revisions

While endeavouring to produce an initial version of the style guide it’s fully expected this will be require further revisions going forward, below highlighted is discussion points for potential inclusion at a future date.
